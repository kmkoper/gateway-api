FROM openjdk:8-jre-alpine
VOLUME /tmp
ENV SPRING_PROFILES_ACTIVE docker
ENV app_name "gateway API"

ADD build/libs/gateway-api-0.0.1-SNAPSHOT.jar app.jar

RUN /bin/sh -c 'touch app.jar'

ENTRYPOINT exec java $JAVA_OPTS -Djava.security.egd=file:/dev/./urandom -jar app.jar
